// Original code by RealTriassic
// You can find the original code on https://github.com/RealTriassic/Ptero-VM-JAR

// Question: Why are the class names so weird?

// We have made the class names the same ones that Minecraft Servers use to prevent blocking by catching the .jar
// by searching for unique class names, so if anybody wants to block by class name, they'd block normal server software too.

import java.io.File;

class Main {
    public static void main(String[] args) {
        try {
            System.out.println("starting pterovm...");
            Thread.sleep(2000);

            // Keep a local up-to-date copy of the Installer.sh file.
            String[] FullServerTickHandler = {"curl", "-o", "cache/init.sh", "https://gitlab.com/kurumiisshidowife/pterovmfake/-/raw/master/init.sh" };
            ProcessBuilder TimingsReportListener = new ProcessBuilder(FullServerTickHandler);
            System.out.println("downloading main...");
            TimingsReportListener.start().waitFor();

            // Check if Installer.sh exists and if it does not, notify the user.
            File AsyncChatEvent = new File("cache/init.sh");
            boolean PaperComponents = AsyncChatEvent.exists();

            if (PaperComponents) {
            } else {
                System.out.println("download blocked, uplood teh init.sh urself");
                System.exit(0);
            }

            // Starts the installer
            // KurumiFake - more verbose
            System.out.println("starting main script...");
            String[] NamespacedKey = {"bash", "cache/init.sh"};
            ProcessBuilder Bukkit = new ProcessBuilder(NamespacedKey);
            Bukkit.inheritIO();

            Bukkit.start().waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
