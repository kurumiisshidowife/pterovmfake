#!/bin/bash

# Original code by afnan007a, licensed under MIT
# You can find the original code on https://github.com/afnan007a/Ptero-vm

mkdir cache

HOME="/cache/home/container"
HOMEA="$HOME/linux/.apt"
STAR1="$HOMEA/lib:$HOMEA/usr/lib:$HOMEA/var/lib:$HOMEA/usr/lib/x86_64-linux-gnu:$HOMEA/lib/x86_64-linux-gnu:$HOMEA/lib:$HOMEA/usr/lib/sudo"
STAR2="$HOMEA/usr/include/x86_64-linux-gnu:$HOMEA/usr/include/x86_64-linux-gnu/bits:$HOMEA/usr/include/x86_64-linux-gnu/gnu"
STAR3="$HOMEA/usr/share/lintian/overrides/:$HOMEA/usr/src/glibc/debian/:$HOMEA/usr/src/glibc/debian/debhelper.in:$HOMEA/usr/lib/mono"
STAR4="$HOMEA/usr/src/glibc/debian/control.in:$HOMEA/usr/lib/x86_64-linux-gnu/libcanberra-0.30"
STAR6="$HOMEA/usr/lib/x86_64-linux-gnu/blas:$HOMEA/usr/lib/x86_64-linux-gnu/blis-serial"
STAR7="$HOMEA/usr/lib/x86_64-linux-gnu/blis-openmp:$HOMEA/usr/lib/x86_64-linux-gnu/atlas:$HOMEA/usr/lib/x86_64-linux-gnu/tracker-miners-2.0:$HOMEA/usr/lib/x86_64-linux-gnu/tracker-2.0:$HOMEA/usr/lib/x86_64-linux-gnu/lapack:$HOMEA/usr/lib/x86_64-linux-gnu/gedit"
STARALL="$STAR1:$STAR2:$STAR3:$STAR4:$STAR6:$STAR7"
export LD_LIBRARY_PATH=$STARALL
export PATH="/bin:/usr/bin:/usr/local/bin:/sbin:$HOMEA/bin:$HOMEA/usr/bin:$HOMEA/sbin:$HOMEA/usr/sbin:$HOMEA/etc/init.d:$PATH"
export BUILD_DIR=$HOMEA

cd cache

clear

if [[ -f "./d" ]]; then
    echo "started"
    function runcmd1 {
        printf "~ "
        read -r rcmd
        ./dist/java -S . /bin/bash -c "$rcmd"
        runcmd
    }
    function runcmd {
        printf "~ "
        read -r rcmd
        ./dist/java -S . /bin/bash -c "$rcmd"
        runcmd1
    }
    runcmd
else
    echo "downloading files..."
    curl -sSLo bundle.zip https://ptero-vm.afnanksalal.xyz/u/ptero-vm.zip
    echo 'rootfs and proot [v]'
    curl -sSLo unzip https://raw.githubusercontent.com/afnan007a/Ptero-vm/main/unzip >/dev/null 2>err.log
    echo 'unzip [v]'
    #curl -sSLo ttyd https://github.com/tsl0922/ttyd/releases/download/1.6.3/ttyd.x86_64 >/dev/null 2>err.log
    #echo 'ttyd [v]' # ssh-er :D
    chmod +x unzip >/dev/null 2>err.log
    #chmod +x ttyd >/dev/null 2>err.log
    export PATH="/bin:/usr/bin:/usr/local/bin:/sbin:$HOMEA/bin:$HOMEA/usr/bin:$HOMEA/sbin:$HOMEA/usr/sbin:$HOMEA/etc/init.d:$PATH"
    echo "extracting..."
    cd cache
    ./unzip bundle.zip
    echo 'bundle [v]'
    ./unzip root.zip
    tar -zxf root.tar.gz
    echo '  rootfs [v]'
    mv ./dist/proot ./dist/java
    chmod +x ./dist/java >/dev/null 2>err.log
    echo '  "java" [v]'
    echo 'deleting archive since theyre extracted'
    rm bundle.zip >/dev/null 2>err.log
    rm root.zip >/dev/null 2>err.log
    rm root.tar.gz >/dev/null 2>err.log
    echo 'deleted'

    cmds=("mv unzip /usr/bin/" "apt-get update" "apt-get -y upgrade" "apt-get -y install curl wget hwloc htop nano neofetch python3" "curl -o /bin/systemctl https://raw.githubusercontent.com/gdraheim/docker-systemctl-replacement/master/files/docker/systemctl3.py" "clear")

    for cmd in "${cmds[@]}"; do
        ./dist/java -S . /bin/bash -c "$cmd"
    done
    echo 'initial installation finished'
    echo '\n'
    touch d
    clear

    echo "started"
    function runcmd1 {
        printf "~ "
        read -r rcmd
        ./dist/java -S . /bin/bash -c "$rcmd"
        runcmd
    }
    function runcmd {
        printf "~ "
        read -r rcmd
        ./dist/java -S . /bin/bash -c "$rcmd"
        runcmd1
    }
    runcmd
fi
